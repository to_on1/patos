public class PatoBlanco extends Pato{
	
	public PatoBlanco(){
		comportamiento = new NoPuedoVolar();
	}
	
	public static void main(String[] args) {
		PatoBlanco p = new PatoBlanco();
		p.volar();
		p.setVuelo(new VolvarConAlas());
		p.volar();
		p.setVuelo(new NoPuedoVolar());
		p.volar();
	}
}
