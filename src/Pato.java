/**
 * Pato andino, blanco, negro, 
 * Grasnido
 */

abstract class Pato {
	
	ComportamientoVuelo comportamiento;
	
	public void mostrar(){
		System.out.println("soy un Pato");
	}
	
	public void volar(){
		comportamiento.volar();
	}
	
	public void setVuelo(ComportamientoVuelo v){
		comportamiento = v;
	}
}
